package ru.zak.fractions;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.System.out;
import static ru.zak.fractions.Rational.scanner;

/**
 * @author  Заставская on 17.02.2017.
 */
public class Demo {
    public static void main(String[] args) throws IOException {
        String str;
        FileWriter fileWriter = new FileWriter("file1.txt");
        BufferedReader bufferedReader = new BufferedReader(new FileReader("file.txt"));
        str = bufferedReader.readLine();
        ischeckup(str);

        while ((str = bufferedReader.readLine()) != null) {


            String[] arr = str.split(" ");
            String one = arr[0];
            String two = arr[2];
            String[] array = one.split("/");
            int numArr[] = new int[array.length];
            for (int j = 0; j < array.length; j++) {
                numArr[j] = Integer.parseInt(array[j]);
            }
            int num = numArr[0];
            int denum = numArr[1];


            String[] arraytwo = two.split("/");
            int numArrTwo[] = new int[arraytwo.length];
            for (int j = 0; j < arraytwo.length; j++) {
                numArrTwo[j] = Integer.parseInt(arraytwo[j]);
            }

            int numtwo = numArrTwo[0];
            int denumtwo = numArrTwo[1];

            Rational rational1 = new Rational(numtwo, denumtwo);
            Rational rational = new Rational(num, denum);
            fileWriter.write(String.valueOf(fraction(rational,rational1,arr[1])));
            fileWriter.append("\n");
        }
        fileWriter.close();
        bufferedReader.close();

    }


    private static boolean ischeckup(String str) {
        Pattern pattern = Pattern.compile("[1-9][0-9]*[/][1-9][0-9]*\\s[+-:*]\\s[1-9][0-9]*[/][1-9][0-9]*");
        Matcher matcher = pattern.matcher(str);
        return matcher.find();
    }
    private static void savearray(int i, String[] strings, String retval) {
        strings[i] = retval;

    }

    /**
     * Метод, который разделяет строку на числитель и знаменатель и сохраняет их в обьекте Fraction.
     *
     * @param i номер элемента массива
     * @param strings массив
     * @param rational обьект
     */
    private static void splitarray(int i, String[] strings, Rational rational) {
        String[] strings1 = new String[2];
        int i1 = 0;
        for (String retval : strings[i].split("/")) {
            savearray(i1, strings1, retval);
            i1++;
        }
        rational.setNum(Integer.parseInt(strings1[0]));
        rational.setDenum(Integer.parseInt(strings1[1]));
    }


    /**
     * Метод для считывания знака между дробями
     * @param rational - первая дробь
     * @param rational1- вторая дробь
     */
    public static Rational fraction(Rational rational, Rational rational1, String string) {
        Rational rational2 = new Rational();
        switch (string) {
            case "+":
                rational2 = rational.addit(rational1);
                return rational2;
            case "-":
                rational2 = rational.subrtrac(rational1);
                return rational2;
            case "*":
                rational2 = rational.multiplic(rational1);
                return rational2;
            case ":":
                rational2 = rational.div(rational1);
                return rational2;


        }
        return null;
    }
}