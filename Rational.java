package ru.zak.fractions;

import java.util.Scanner;

/**
 * Класс служит для действий над дробями
 * @author Заставская on 17.02.2017.
 */
public class Rational {
    static Scanner scanner = new Scanner(System.in);
    private int num;
    private int denum;


    public Rational(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }

    public Rational() {
        this(1, 1);
    }

    public Rational(int num) {
        this(num, 1);
    }

    @Override
    public String toString() {
        return "{" + num +
                "/" + denum +
                '}';
    }


    public void setNum(int num) {
        this.num = num;
    }

    public void setDenum(int denum) {
        this.denum = denum;
    }

    /**
     * Метод для сложения двух дробей.
     * @return - возвращает дробь обьектом
     */
    public Rational  addit(Rational rational1) {
        int x; int y;
        if (this.denum == rational1.denum) {
             x = (this.num + rational1.num);
             y = denum;
        } else {
             x = (rational1.num * this.denum + this.num * rational1.denum);
             y = this.denum * rational1.denum;
        }

        return new Rational(x,y);
    }

    /**
     * Метод для вычисления разности двух дробей
     * @return - возвращает дробь обьектом
     */


    public Rational subrtrac(Rational rational1) {
        int x;
        int y;
        if (this.denum == rational1.denum) {
            x = (this.num - rational1.num);
            y = denum;
        } else {
            x = (this.num * rational1.denum - rational1.num * this.denum);
            y = (this.denum * rational1.denum);
        }

        return new Rational(x, y) ;
    }

    /**
     * Метод для вычисления произведения дробей
     *
     * @return - возвращает дробь обьектом
     */

    public Rational multiplic(Rational rational1) {
        int x;
        int y;
        x = this.num * rational1.num;
        y = rational1.denum * this.denum;

        return new Rational(x, y);
    }

    /**
     * Метод для вычисления частного двух дробей
     * @return - возвращает дробь обьектом.
     */


    public Rational div(Rational rational1) {
        int x;
        int y;
        x = this.num * rational1.denum;
        y = this.denum * rational1.num;

        return new Rational(x,y) ;
    }

}
